## The Python Learner

1. This project Aims to create a word-cloud in angular using twitter feed of the user(s).
2. We will also annalyse the tweets and on basis of that we will give prediction of the new tweet by a certain user.
3. We will be using the following. 
  * Angular 
  * NodeJs
  * Python (with NLP and tensorflow)
  * MariaDB
4. The objective of this is to create a framework for a user where they can analyse their social media interactions.
